package com.ood.device.hik.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 公告消息
 *
 * @author zsj
 * @date 2024/12/20
 */
@Data
public class NoticeEntity {

    public static final int LEVEL_ADV = 1;
    public static final int LEVEL_ESTATE = 2;
    public static final int LEVEL_WARN = 3;
    public static final int LEVEL_NOTICE = 4;

    /**
     * 公告编号
     */
    private String number;
    /**
     * 公告主题
     */
    private String theme;
    /**
     * 公告日期
     */
    private Date date;
    /**
     * 公告详情
     */
    private String detail;
    /**
     * 公告等级
     */
    private Integer level;
    /**
     * 公告图片数量
     */
    private Integer picNum;
    /**
     * 图片信息
     */
    private List<NoticePic> pics;

    @Data
    public static class NoticePic {
        private String picData;

    }
}
